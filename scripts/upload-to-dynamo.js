const fs = require('fs');
const util = require('util');
const { putMovie, getMovie, putMovieShort, getMovieShort, getAllMovies, TableGetPage } = require( './dynamo');


const readFile = util.promisify(fs.readFile);

async function uploadMovieDetails() {
    const movieDetails = JSON.parse(await readFile('./db__.json')).movies;
    console.log(`Movies available: ${movieDetails.length}`);
    console.log('adding new movies to the table..')
    movieDetails.forEach(async function PutMovieIfExists(movie){
        const existingMovie = await getMovie(movie.id);
        if (existingMovie) {
            console.log(`${movie.title} already exists, skipping..`)
        } else {
            await putMovie(movie);
            console.log(`${movie.title} is persisted now!`)
        }
    });
}

async function uploadMoviesPerGenre (genre) {
    const movieDetails = JSON.parse(await readFile('./db__.json')).movies;
    const genreMovies = movieDetails.filter( movie => movie.genres.find( g => g.name === genre ) );
    console.log(`Found ${genreMovies.length} "${genre}" movies!`)
    const promises = genreMovies.map(async (details) => {
        const dynamoId = `${genre}_${details.id}`;
        const movieShort = await getMovieShort(dynamoId);
        if (movieShort) {
            console.log(`Skipping ${movieShort.title}`);
            return Promise.resolve();
        } else {
            const movieShort = {
                title: details.original_title || details.title,
                id: details.id,
                release_date: details.release_date,
                poster_path: details.poster_path,
                rating: details.my_rating,
                movielens_average_rating: details.movielens_average_rating,
                duration: details.runtime,
                vote_average: details.vote_average,
                vote_count: details.vote_count,
                popularity: details.popularity,
                dynamo: dynamoId
            };
            return putMovieShort(movieShort);
        }
       
    });
    return Promise.all(promises);
    
}

// uploadMovieDetails();
// TableGetPage('MoviePerGenre', 'Crime').then(res=> {
//     console.log(res);
// })

['Action', 'Crime', 'Comedy', 'Animation', 'Documentary'].forEach(
    g => uploadMoviesPerGenre(g)
);