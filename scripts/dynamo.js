const AWS = require('aws-sdk');
const MOVIES_TABLE = "MovieDetails",
MOVIES_PER_GENRE_TABLE = "MoviePerGenre2";

// uncomment to print queries and other debug stuff to console
// AWS.config.logger = console;

const docClient = new AWS.DynamoDB.DocumentClient({ region: 'us-east-2' });

async function TableGetAll(tableName) {
    return new Promise((resolve, reject) => {
        const params = { TableName: tableName }
        docClient.scan(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                // continue scanning if we have more users, because
                // scan can retrieve a maximum of 1MB of data
                // TODO: gotta test it later, lol
                if (typeof data.LastEvaluatedKey != "undefined") {
                    reject(new Error(`More than 1MB of ${tableName}!`));
                }
                // print all the movies
                resolve(data.Items);
            };
        });
    })
}

async function TableGet(tableName, id, idParamName = "id") {
    return new Promise((resolve, reject) => {
        const params = {
            TableName: tableName, Key: {
                [idParamName]: id
            }
        }
        docClient.get(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data.Item);
            };
        });
    })
}

async function TableUpdate(tableName, id, diff) {
    //todo: concat strings and shit
    return new Promise((resolve, reject) => {
        const updateExpression = `set ${Object.keys(diff).map( key => `${key} = :${key}` ).join(', ')}`;
        const expressionAttributeValues = {};
        Object.keys(diff).forEach(key => {
           expressionAttributeValues[`:${key}`] = diff[key]
        })

        var params = {
            TableName: tableName,
            Key: {
                "id": id
            },
            UpdateExpression: updateExpression,
            ExpressionAttributeValues: expressionAttributeValues,
            ReturnValues: "UPDATED_NEW"
        };

        docClient.update(params, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

async function TablePut(tableName, entity) {
    var params = {
        TableName: tableName,
        Item: entity
    };
    return new Promise((resolve, reject)=>{
        docClient.put(params, function(err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data.Item);
            }
        });
    });
}

async function TableGetPage(tableName, partOfId, lastEvaluatedKey) {
    return new Promise((resolve, reject) => {
        const params = { 
            TableName: tableName,
            FilterExpression: "begins_with(#id, :partOfId)",
            ExpressionAttributeNames: {
                "#id": "dynamo",
            },
            ExpressionAttributeValues: {
                 ":partOfId": partOfId
            }
        };
        if (lastEvaluatedKey) {
            params[LastEvaluatedKey] = lastEvaluatedKey;
        }
        docClient.scan(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({
                    lastEvaluatedKey: data.LastEvaluatedKey,
                    items: data.Items
                });
            };
        });
    })
}

module.exports = {
    getAllMovies() {
        return TableGetAll(MOVIES_TABLE);
    },

    getMovie(id) {
        return TableGet(MOVIES_TABLE, id);
    },

    updateMovie(id, diff) {
        return TableUpdate(MOVIES_TABLE, id, diff);
    },

    putMovie(movie) {
        return TablePut(MOVIES_TABLE, movie);
    },

    putMovieShort(movie) {
        return TablePut(MOVIES_PER_GENRE_TABLE, movie);
    },

    getMovieShort(id) {
        return TableGet(MOVIES_PER_GENRE_TABLE, id, "dynamo");
    },
    TableGetPage
};