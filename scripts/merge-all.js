const fs = require('fs');
const path = require('path');
const util = require('util');

const write = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const readdir = util.promisify(fs.readdir);

async function mergeDowloadedJsons() {
    const movieObjects = [], movieShortObjects = [];
    const myRatings = JSON.parse((await readFile('db.json'))).movies;
    const files = await readdir('mock');
    await Promise.all(files.map(fileName => {
        return readFile(`mock/${fileName}`).then(fileContent => {
            const details = JSON.parse(fileContent).data;
            const rating = myRatings.find(m => m.tmdb_id === "" + details.id);
            if (!rating) {
                console.log(`No rating for ${details.title}`);
            }
            if (Number(rating.rating) < 3.5) { return; }
            details.my_rating = rating.rating;
            details.movielens_average_rating = rating.average_rating;
            movieObjects.push(details);
            movieShortObjects.push({
                title: details.original_title || details.title,
                id: details.id,
                release_date: details.release_date,
                poster_path: details.poster_path,
                rating: rating.rating
            });
        })
    }))
    write('db__.json', JSON.stringify({
        movies: movieObjects,
        movies_short: movieShortObjects
    }, null, 2));
}

mergeDowloadedJsons();
