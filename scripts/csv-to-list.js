require('dotenv').config()

const fs = require('fs');
const util = require('util');

const csvtojsonV2 = require("csvtojson/v2");
const MovieDB = require('node-themoviedb');

const MOVIELENS_FILE_PATH = 'movielens-ratings.csv';

const write = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

async function csvToJson() {
    return csvtojsonV2()
        .fromFile(MOVIELENS_FILE_PATH)
        .then((jsonObj) => {
            let sorted = jsonObj
                .sort((a, b) => b.rating - a.rating)
                .map(v => ({ ...v, id: v.movie_id }));
            return write('db.json', JSON.stringify({ movies: sorted }, null, 2)).then(() => sorted)
        })
}

async function downloadTmdbDetails(sortedMovies) {
    console.log(process.env.TMDB_API_KEY);
    const existingData = JSON.parse(await readFile('db__.json')).movies;

    const mdb = new MovieDB(process.env.TMDB_API_KEY);
    for (var i = 0; i < sortedMovies.length; i++) {
        const movie = sortedMovies[i];
        if (existingData.find(details => details.id == movie.tmdb_id)) {
            console.log(`Skipping ${movie.title}`)
            continue;
        }
        const args = {
            pathParameters: {
                movie_id: movie.tmdb_id,
            },
        };
        try {
            console.log(`https://www.themoviedb.org/movie/${movie.tmdb_id}`)
            const movieDetails = await mdb.movie.getDetails(args);
            write(`mock/${movie.title.replace(/[^a-z0-9]/gi, '_').toLowerCase()}-${movie.tmdb_id}.json`, JSON.stringify(movieDetails, null, 2));
        } catch (e) {
            console.error(e)
        }
        await sleep(5000);
        console.log(`${i++}. Downloaded meta for ${movie.title}`);
    }
}

(async function run() {
    const sortedMovies = await csvToJson();
    await downloadTmdbDetails(sortedMovies);
}())
