class MovieService {

    constructor(jsonBackendUrl) {
        this.baseUrl = jsonBackendUrl;
    }

    getAllMoviesCount() {
        return fetch(`${this.baseUrl}/movies`)
            .then(response => response.json()).then((arr) => arr.length);
    }
    getAllMoviesFiltered(filters = []) {
        return fetch(`${this.baseUrl}/movies`)
            .then(response => response.json())
            .then((movies) => movies.filter((movie) => {
                if (filters.length === 0) {
                    return true;
                } else {
                    return movie.genres.find(genre => filters.includes(genre.name));
                }
            }).sort((a,b) => b.my_rating -  a.my_rating)
            )
    }

    getMovie(movieId) {
        return fetch(`${this.baseUrl}/movies/${movieId}`)
    }
}

const instance = new MovieService('http://localhost:3000');

export default instance;