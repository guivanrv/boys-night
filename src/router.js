import VueRouter from 'vue-router'

import MovieList from './components/MovieList.vue';
import MovieDetails from './components/MovieDetails.vue';

const routes = [
    { path: '', component: MovieList },
    { path: '/details/:id', component: MovieDetails }
]

export default new VueRouter({
    routes
})