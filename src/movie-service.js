class MovieService {

    constructor(backendUrl) {
        this.baseUrl = backendUrl;
    }

    getAllMoviesCount() {
        return Promise.resolve('TBD');
    }
    getAllMoviesFiltered(filter = '*', lastLineKey = '') {
        return fetch(`${this.baseUrl}/movies_short/${filter}/${lastLineKey}`, { mode: 'cors'})
            .then(response => response.json())
            .then(movies => movies.items)
            .then(items => {
                if (filter !== '*') {
                    return items;
                } else {
                    // removing dupes
                    let original = [];
                    items.forEach( item => {
                        if (original.find (mov => mov.id === item.id)) {
                            return;
                        }
                        original.push(item);
                    } )
                    return original;
                }
            })
    }

    getMovie(movieId) {
        return fetch(`${this.baseUrl}/movies/${movieId}`, { mode: 'cors'}).then(response => response.json())
    }
}

const instance = new MovieService('https://vurinz5sp2.execute-api.us-east-2.amazonaws.com/dev');

export default instance;